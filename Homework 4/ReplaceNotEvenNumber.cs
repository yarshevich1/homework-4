﻿namespace Homework_4
{
    internal class ReplaceNotEvenNumber
    {
        //Создайте массив и заполните массив.
        //    Выведите массив на экран в строку.
        //    Замените каждый элемент с нечётным индексом на ноль.
        //    Снова выведете массив на экран на отдельной строке. 

        public static void ReplaceOddNumbers()
        {
            while (true)
            {
                Console.WriteLine("Generated array:");
                int[] randomArray = Helper.GenerateArray();
                Helper.PrintArray(randomArray);

                int[] tempArray = randomArray.Select(x => x % 2 != 0 ? 0 : x).ToArray();
                Helper.PrintArray(tempArray);

                Console.ReadKey();
            }
        }
    }
}
