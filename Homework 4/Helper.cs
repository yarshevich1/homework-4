﻿namespace Homework_4
{
    internal class Helper
    {
        static Random _rnd = new Random(DateTime.Now.Second);
        /// <summary>
        /// Вывод массива на консоль
        /// </summary>
        /// <param name="array">Массив котоырй нужно вывести</param>
        public static void PrintArray(int[] array) => PrintMessageToConsole(string.Join(" ", array.Select(x => x.ToString())), ConsoleColor.Cyan);

        /// <summary>
        /// Генерация случайного массива
        /// </summary>
        /// <returns>Возвращает случайный массив int[]</returns>
        public static int[] GenerateArray()
        {
            // создаем массив от 5 до 11 символов
            int[] array = new int[_rnd.Next(5, 11)];

            // заполняем произволиными числами
            array = array.Select(x => _rnd.Next(3, 9)).ToArray();

            // возвращаем
            return array;
        }

        /// <summary>
        /// Генерация случайного массива
        /// </summary>
        /// <param name="sizeArray">размер массива, котоырй нужно создать</param>
        /// <returns>Возвращает случайный массив int[]</returns>
        public static int[] GenerateArray(int sizeArray)
        {
            // создаем массив указанной длинны
            int[] array = new int[sizeArray];

            // заполняем произволиными числами
            array = array.Select(x => _rnd.Next(3, 9)).ToArray();

            // возвращаем
            return array;
        }

        /// <summary>
        /// Создание матрицы
        /// </summary>
        /// <param name="sizeMatrix">Размер матрицы</param>
        /// <returns>Возвращает матрицу заполненную рандомными элементами</returns>
        public static int[,] GenerateMatrix(int sizeMatrix)
        {
            // Объявляем матрицу с указаыынм размером
            int[,] matrix = new int[sizeMatrix, sizeMatrix];

            // Проходим по строкам
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                // Проходим по столбцам
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    // Заполняем рандомными значениями
                    matrix[i, j] = _rnd.Next(1, 10);
                }
            }

            // возвращаем
            return matrix;
        }

        /// <summary>
        /// Вывоз матрицы на консоль
        /// </summary>
        /// <param name="matrix"></param>
        public static void PrintMatrix(int[,] matrix)
        {
            // Проходим по строкам
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                // Проходим по столбцам
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    // Выводим на консоль
                    PrintMessage($"{matrix[i, j]} ", ConsoleColor.Cyan);
                }
                // Переход на новую строку в конце строки
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Вывод сообщения на консоль
        /// </summary>
        /// <param name="message">Сообщение для вывода</param>
        /// <param name="consoleColor">Цвет текста</param>
        public static void PrintMessageToConsole(string message, ConsoleColor consoleColor)
        {
            Console.ForegroundColor = consoleColor;
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.White;
        }

        /// <summary>
        /// Вывод сообщения на консоль в текущей строке
        /// </summary>
        /// <param name="message">Сообщение для вывода</param>
        /// <param name="consoleColor">Цвет текста</param>
        public static void PrintMessage(string message, ConsoleColor consoleColor)
        {
            Console.ForegroundColor = consoleColor;
            Console.Write(message);
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
