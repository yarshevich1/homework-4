﻿namespace Homework_4
{
    internal class DoubleArray
    {
        //Создайте 2 массива из 5 чисел.
        //Выведите массивы на консоль в двух отдельных строках.
        //Посчитайте среднее арифметическое элементов каждого массива и сообщите, для какого из массивов это значение оказалось больше (либо сообщите, что их средние арифметические равны).  
        public static void GetAverageValueArray()
        {
            while (true)
            {
                Console.WriteLine("Generated first array:");
                int[] firstArray = Helper.GenerateArray(5);
                Helper.PrintArray(firstArray);

                Console.WriteLine("Generated second array:");
                int[] secondArray = Helper.GenerateArray(5);
                Helper.PrintArray(secondArray);

                Console.WriteLine($"Average first array is {firstArray.Average()}");
                Console.WriteLine($"Average second array is {secondArray.Average()}");

                Console.WriteLine(firstArray.Average() == secondArray.Average() ?
                    $"Average first array equal second array" :
                    firstArray.Average() > secondArray.Average() ?
                        $"Average first array is more then second array" :
                        $"Average second array is more then first array");

                Console.ReadKey();
            }
        }
    }
}
