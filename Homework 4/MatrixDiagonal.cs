﻿namespace Homework_4
{
    internal class MatrixDiagonal
    {
        //Создайте двумерный массив.Выведите на консоль диагонали массива.

        public static void GetMatrixDiagonalNumbers()
        {
            Console.WriteLine("Generated matrix:");
            int[,] matrix = Helper.GenerateMatrix(5);
            Helper.PrintMatrix(matrix);

            PrintMainDiagonal(matrix);
            PrintSecondaryDiagonal(matrix);

            Console.ReadKey();

        }

        static void PrintMainDiagonal(int[,] matrix)
        {

            //  Получаем минимальное значение между количеством строк и столбцов массива
            int size = Math.Min(matrix.GetLength(0), matrix.GetLength(1));

            for (int i = 0; i < size; i++)
            {
                Console.Write(matrix[i, i] + " ");
            }
            Console.WriteLine();
        }

        static void PrintSecondaryDiagonal(int[,] matrix)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                Console.Write(matrix[i, matrix.GetLength(1) - i - 1] + " ");
            }
            Console.WriteLine();
        }
    }
}
