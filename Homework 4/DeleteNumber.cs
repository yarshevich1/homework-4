﻿namespace Homework_4
{
    internal class DeleteNumber
    {
        //Создайте массив целых чисел.Удалите все вхождения заданного числа из массива.  
        //Пусть число задается с консоли.Если такого числа нет - выведите сообщения об этом.  
        //В результате должен быть новый массив без указанного числа.

        public static void DeleteNumberFormArray()
        {
            while (true)
            {
                Console.WriteLine("Generated array:");
                int[] randomArray = Helper.GenerateArray();

                Helper.PrintArray(randomArray);
                Console.Write("Input number to delete: ");

                if (!int.TryParse(Console.ReadLine(), out var number))
                    Console.WriteLine("You must input only number.");

                List<int> list = new List<int>();

                bool isFound = false;
                foreach (var numb in randomArray)
                {
                    if (numb == number)
                    {
                        isFound = true;
                        continue;
                    }
                    list.Add(numb);
                }

                if (isFound)
                {
                    Helper.PrintMessageToConsole($"The number {number} was delete in the array", ConsoleColor.Green);

                    Helper.PrintMessageToConsole("New array:", ConsoleColor.Magenta);
                    int[] newArray = list.ToArray();
                    Helper.PrintArray(newArray);
                }
                else
                    Helper.PrintMessageToConsole($"The number {number} wasn't found in the array", ConsoleColor.Red);
            }

        }
    }
}
