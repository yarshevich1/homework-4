﻿namespace Homework_4
{
    internal class Program
    {
        static void Main(string[] args)
        {
            FindNumber.FindNumberInArray();
            DeleteNumber.DeleteNumberFormArray();
            MaxAndMin.GetMathArray();
            DoubleArray.GetAverageValueArray();
            SortEvenArray.SortArray();
            ReplaceNotEvenNumber.ReplaceOddNumbers();
            SortStringArray.SortArray();
            BubbleSort.BubbleSortArray(Helper.GenerateArray(7));
            MatrixSum.GetMatrixSum();
            MatrixDiagonal.GetMatrixDiagonalNumbers();
        }
    }
}