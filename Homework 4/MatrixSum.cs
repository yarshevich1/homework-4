﻿namespace Homework_4
{
    internal class MatrixSum
    {
        //Создайте двумерный массив целых чисел.Выведите на консоль сумму всех
        //    элементов массива

        public static void GetMatrixSum()
        {
            Console.WriteLine("Generated matrix:");
            int[,] matrix = Helper.GenerateMatrix(5);
            Helper.PrintMatrix(matrix);

            //Получение суммы всех элементов массива
            int sum = 0;
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    sum += matrix[i, j];
                }
            }

            Helper.PrintMessageToConsole($"Sum array is {sum}", ConsoleColor.Green);
            Console.ReadKey();
        }
        
    }
}
