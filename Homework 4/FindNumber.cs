﻿namespace Homework_4
{
    internal class FindNumber
    {
        //Создайте массив целых чисел.Напишете программу, которая выводит сообщение о том, входит ли заданное число в массив или нет.  Пусть число для поиска задается с консоли.

        public static void FindNumberInArray()
        {
            while (true)
            {
                Console.WriteLine("Generated array:");
                int[] randomArray = Helper.GenerateArray();

                Helper.PrintArray(randomArray);
                Console.Write("Input number to search: ");

                if (!int.TryParse(Console.ReadLine(), out var number))
                    Console.WriteLine("You must input only number.");

                bool isFound = false;
                foreach (var num in randomArray)
                {
                    if (num == number)
                    {
                        isFound = true;
                        break;
                    }
                }

                if (isFound)
                    Helper.PrintMessageToConsole($"The number {number} was found in the array", ConsoleColor.Green);
                else
                    Helper.PrintMessageToConsole($"The number {number} wasn't found in the array", ConsoleColor.Red);

                Console.ReadKey();
            }
        }
    }
}
