﻿namespace Homework_4
{
    internal class BubbleSort
    {
        //Реализуйте алгоритм сортировки пузырьком.

        public static void BubbleSortArray(int[] inputArray)
        {
            Console.WriteLine("Input array: ");
            Helper.PrintArray(inputArray);

            for (int i = 0; i < inputArray.Length - 1; i++)
            {
                for (int j = 0; j < inputArray.Length - i - 1; j++)
                {
                    if (inputArray[j] > inputArray[j + 1])
                    {
                        int temp = inputArray[j];
                        inputArray[j] = inputArray[j + 1];
                        inputArray[j + 1] = temp;
                    }
                }
            }

            Console.WriteLine("Sort array: ");
            Helper.PrintArray(inputArray);
        }
    }
}
