﻿namespace Homework_4
{
    internal class SortStringArray
    {
        //Создайте массив строк.Заполните его произвольными именами людей.
        //Отсортируйте массив.
        //Результат выведите на консоль.

        public static void SortArray()
        {
            Helper.PrintMessageToConsole("Generated array:", ConsoleColor.Red);
            string[] namesArray = { "Яна", "Петя", "Анна", "Григорий", "Василий", "Василиса", "Олег", "Александра", "Дмитрий", " Ангелина" };
            Console.WriteLine(string.Join(" ", namesArray.Select(x => x)));

            Array.Sort(namesArray);
            Helper.PrintMessageToConsole("Sorted array:", ConsoleColor.Cyan);
            Console.WriteLine(string.Join(" ", namesArray.Select(x => x)));

            Console.ReadKey();
        }
    }
}
