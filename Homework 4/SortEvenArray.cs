﻿namespace Homework_4
{
    internal class SortEvenArray
    {
        //Создайте массив из n случайных целых чисел и выведите его на экран.
        //Размер массива пусть задается с консоли и размер массива может быть больше 5 и меньше или равно 10.  
        //Если n не удовлетворяет условию - выведите сообщение об этом.  Если пользователь ввёл не подходящее число, то программа должна просить пользователя повторить ввод.
        //Создайте второй массив только из чётных элементов первого массива, если они там есть, и вывести его на экран.

        public static void SortArray()
        {
            while (true)
            {
                int sizeArray = 0;
                do
                {
                    Console.WriteLine("Enter the size of the array from 5 to 10");
                } while (!int.TryParse(Console.ReadLine(), out sizeArray) || !ValidateSizeArray(sizeArray));

                Console.WriteLine("Generated first array:");
                int[] firstArray = Helper.GenerateArray(sizeArray);
                Helper.PrintArray(firstArray);

                List<int> tempArray = firstArray.Where(x => x % 2 == 0).ToList();

                if (tempArray.Count > 0)
                {
                    int[] evenArray = tempArray.ToArray();
                    Helper.PrintArray(evenArray);
                }
                else
                    Helper.PrintMessageToConsole("Even numbers not found", ConsoleColor.Red);

            }
        }

        static bool ValidateSizeArray(int size)
        {
            if (size > 5 && size <= 10)
                return true;
            else
                Console.WriteLine("The number must be from 6 to 10");
            return false;
        }
    }
}
