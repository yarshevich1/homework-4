﻿namespace Homework_4
{
    internal class MaxAndMin
    {
        //Создайте и заполните массив случайным числами и выведете максимальное, минимальное и среднее значение.
        // Для генерации случайного числа используйте метод Random() .  Пусть будет возможность создавать массив произвольного размера.Пусть размер массива вводится с консоли.  

        public static void GetMathArray()
        {
            while (true)
            {
                Console.WriteLine("Enter the size of the array to generate:");

                if (!int.TryParse(Console.ReadLine(), out var sizeArray))
                    Console.WriteLine("You must input only number.");

                Console.WriteLine("Generated array:");
                int[] randomArray = Helper.GenerateArray(sizeArray);
                Helper.PrintArray(randomArray);

                Helper.PrintMessageToConsole($"Max value {randomArray.Max()}", ConsoleColor.Red);
                Helper.PrintMessageToConsole($"Min value {randomArray.Min()}", ConsoleColor.Green);
                Helper.PrintMessageToConsole($"Average value {randomArray.Average()}", ConsoleColor.Yellow);
            }
        }
    }
}
